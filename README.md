Binder系列
---

[Binder系列—开篇](http://gityuan.com/2015/10/31/binder-prepare/)  
[Binder系列1—Binder Driver初探](http://gityuan.com/2015/11/01/binder-driver/)  
[Binder系列2—Binder Driver再探](http://gityuan.com/2015/11/02/binder-driver-2/)  
[Binder系列3—启动Service Manager](http://gityuan.com/2015/11/07/binder-start-sm/)  
[Binder系列5—注册服务(addService)](http://gityuan.com/2015/11/14/binder-add-service/)  
[Binder系列6—获取服务(getService)](http://gityuan.com/2015/11/15/binder-get-service/)  
[Binder系列7—framework层分析](http://gityuan.com/2015/11/21/binder-framework/)  
[Binder系列8—如何使用Binder](http://gityuan.com/2015/11/22/binder-use/)  
[Binder系列9—如何使用AIDL](http://gityuan.com/2015/11/23/binder-aidl/)  
[Binder系列10—总结](http://gityuan.com/2015/11/28/binder-summary/)  


Android媒体开发
---

[Android多媒体开发(一)----MediaPlayer框架开始](http://windrunnerlihuan.com/2016/11/28/Android%E5%A4%9A%E5%AA%92%E4%BD%93%E5%BC%80%E5%8F%91-%E4%B8%80-MediaPlayer%E6%A1%86%E6%9E%B6%E5%BC%80%E5%A7%8B/)  
[Android多媒体开发(二)----MediaPlayer的C/S架构以及C++层调用步骤](http://windrunnerlihuan.com/2016/11/30/Android%E5%A4%9A%E5%AA%92%E4%BD%93%E5%BC%80%E5%8F%91-%E4%BA%8C-MediaPlayer%E7%9A%84C-S%E6%9E%B6%E6%9E%84%E4%BB%A5%E5%8F%8AC-%E5%B1%82%E8%B0%83%E7%94%A8%E6%AD%A5%E9%AA%A4/)  
[Android多媒体开发(三)----从StageFright到AwesomePlayer](http://windrunnerlihuan.com/2016/12/11/Android%E5%A4%9A%E5%AA%92%E4%BD%93%E5%BC%80%E5%8F%91-%E4%B8%89-%E4%BB%8EStageFright%E5%88%B0AwesomePlayer/)  
[Android多媒体开发(四)----AwesomePlayer数据源处理](http://windrunnerlihuan.com/2016/12/12/Android%E5%A4%9A%E5%AA%92%E4%BD%93%E5%BC%80%E5%8F%91-%E5%9B%9B-AwesomePlayer%E6%95%B0%E6%8D%AE%E6%BA%90%E5%A4%84%E7%90%86/)  
[Android多媒体开发(五)----OpenMax简介](http://windrunnerlihuan.com/2016/12/15/Android%E5%A4%9A%E5%AA%92%E4%BD%93%E5%BC%80%E5%8F%91-%E4%BA%94-OpenMax%E7%AE%80%E4%BB%8B/)  
[Android多媒体开发(六)----Android中OpenMax的实现(preview)](http://windrunnerlihuan.com/2016/12/26/Android%E5%A4%9A%E5%AA%92%E4%BD%93%E5%BC%80%E5%8F%91-%E5%85%AD-Android%E4%B8%ADOpenMax%E7%9A%84%E5%AE%9E%E7%8E%B0-preview/)  
[Android多媒体开发(七)----Android中OpenMax的实现](http://windrunnerlihuan.com/2016/12/29/Android%E5%A4%9A%E5%AA%92%E4%BD%93%E5%BC%80%E5%8F%91-%E4%B8%83-Android%E4%B8%ADOpenMax%E7%9A%84%E5%AE%9E%E7%8E%B0/)  


Inputt系统
---

[Input系统—ANR原理分析](http://gityuan.com/2017/01/01/input-anr/)  
[Input系统—启动篇](Input系统—启动篇)  
[Input系统—InputReader线程](http://gityuan.com/2016/12/11/input-reader/)  
[Input系统—InputDispatcher线程](http://gityuan.com/2016/12/17/input-dispatcher/)  
[Input系统—UI线程](http://gityuan.com/2016/12/24/input-ui/)  
[Input系统—进程交互](http://gityuan.com/2016/12/31/input-ipc/)